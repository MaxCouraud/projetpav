/*Création de la base*/

CREATE DATABASE pav

/*Création de la table  User
  Le champ type_user peux prendre 3 valeurs :
    -1 SuperAdmin
    -2 Admin
    -3 Agent
*/

CREATE TABLE pav_user
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    nom VARCHAR(100),
    prenom VARCHAR(100),
    email VARCHAR(255),
    password VARCHAR(25),
    ville VARCHAR(255),
    code_postal VARCHAR(5),
    type_user INT
)

/* INSERT d'un agent type*/

INSERT into pav_user values (1, 'Jean-Michel', 'Agentman',  'agent@agent.com', 'password', 'Bordeaux', '33000', 3)

/* INSERT d'un admin type*/

INSERT into pav_user values (2, 'Jean-Pascal', 'Adminman',  'admin@admin.com', 'password', 'Bordeaux', '33000', 2)

/* INSERT d'un superAdmin type*/

INSERT into pav_user values (3, 'Jean-Patrick', 'Superadminman',  'superadmin@superadmin.com', 'password', 'Bordeaux', '33000', 1)
