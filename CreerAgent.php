<?php
namespace App;

include_once("./Config/Header.php"); ?>

<html>

<?php include_once("./Components/Navbar.php");
if(!empty($_POST)){

    $errors  = array();

    //connexion à la bdd
    $data = new \PDO('mysql:dbname=pav;host=localhost', 'root', '');

    //générer les erreurs, sans cette phrase pdo ne renvoie pas l'exception..
    $data->setAttribute(\PDO::ATTR_ERRMODE,  \PDO::ERRMODE_EXCEPTION);

    //formatage donnée
    $data->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ);



    //NOM
    if(empty($_POST['nom']) || !preg_match('/^[a-zA-Z_]+$/', $_POST['nom'])){
        $errors['nom'] = "Votre nom n'est pas valide. Seuls les lettres sont autorisés.";
    }
    //PRENOM
    if(empty($_POST['prenom']) || !preg_match('/^[a-zA-Z_]+$/', $_POST['prenom'])){
        $errors['prenom'] = "Votre nom n'est pas valide. Seuls les lettres sont autorisés.";
    }
    //EMAIL
    if(empty($_POST['email']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
        $errors['email'] = "Votre email n'est pas valide.";
    }else{
        $req = $data->prepare('SELECT id FROM pav_user WHERE email=?');
        $req->execute([$_POST['email']]);
        $email =  $req->fetch();
        if($email){
            $errors['email']  = 'Email déjà prise pour un autre compte.';
        }
    }
    //PASSWORD
    if(empty($_POST['password']) || $_POST['password'] != $_POST['conf_password']){
        $errors['password'] = "Vous devez rentrer un mot de passe valide et correspondant à la confirmation.";
    }
    //VILLE
    if(empty($_POST['ville']) || !preg_match('/^[a-zA-Z_]+$/', $_POST['ville'])){
        $errors['ville'] = "Votre Ville n'est pas valide. Seuls les lettres sont autorisés.";
    }
    //CODE POSTAL
    if(empty($_POST['code_postal']) || !preg_match('/^[0-9_]+$/', $_POST['code_postal'])){
        $errors['code_postal'] = "Votre code postal n'est pas valide. Seuls les chiffres sont autorisés.";
    }

    //fonction de création de token
    function str_random($length){
        $alphabet = "0123456789azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN";
        return substr(str_shuffle(str_repeat($alphabet, $length)), 0, $length);
    }

    if(empty($errors)){
        //connexion à la bdd
        $data = new \PDO('mysql:dbname=pav;host=localhost', 'root', '');

        //générer les erreurs, sans cette phrase pdo ne renvoie pas l'exception..
        $data->setAttribute(\PDO::ATTR_ERRMODE,  \PDO::ERRMODE_EXCEPTION);

        //formatage donnée
        $data->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ);

        $req =  $data->prepare("INSERT into pav_user SET nom = ?, prenom  = ?, email = ?, password = ?, ville = ?, code_postal = ?, type_user = ?");

        //Pour envoyé un password crypté à la bdd
        $password = password_hash($_POST['password'], PASSWORD_BCRYPT);

        $req->execute([$_POST['nom'], $_POST['prenom'], $_POST['email'] , $password , $_POST['ville'],  $_POST['code_postal'], $_POST['type_user'] = 3]);

        header('Location: Connexion.php');
        exit();
    }
}

?>

<div>
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link" href="AdminPage.php">Créer Pav</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="AdministrerPav.php">Administer Pav</a>
        </li>
        <li class="nav-item">
            <a class="nav-link active" href="CreerAgent.php">Créer Agent</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="AdministrerAgent.php">Administer Agent</a>
        </li>
    </ul>
    <?php if(!empty($errors)): ?>
    <div class="alert alert-danger">
        <p>Vous n'avez pas rempli le formulaire correctement</p>
        <ul>
            <?php foreach ($errors as $error): ?>
                <li><?= $error; ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
    <?php endif; ?>
    <div class="col-12 col-sm-4">
        <form action="" method="POST">
            <div  class="form-group">
                <label for="">Nom</label>
                <input type="text" name="nom" class="form-control" placeholder="Entrez votre nom" required>
            </div>
            <div  class="form-group">
                <label for="">Prénom</label>
                <input type="text" name="prenom" class="form-control" placeholder="Entrez votre prénom" required>
            </div>
            <div  class="form-group">
                <label for="">Email</label>
                <input type="email" name="email" class="form-control" placeholder="Entrez votre email" required>
            </div>
            <div  class="form-group">
                <label for="">Mot de passe</label>
                <input type="password" name="password" class="form-control" placeholder="Entrez votre mot de passe" required>
            </div>
            <div  class="form-group">
                <label for="">Confirmation mot de passe</label>
                <input type="password" name="conf_password" class="form-control" placeholder="Entrez votre mot de passe" required>
            </div>
            <div  class="form-group">
                <label for="">Ville</label>
                <input type="text" name="ville" class="form-control" placeholder="Entrez votre ville" required>
            </div>
            <div  class="form-group">
                <label for="">Code Postal</label>
                <input type="text" name="code_postal" class="form-control" placeholder="Entrez votre code postal" required>
            </div>

            <button type="submit" class="btn btn-primary">Créer</button>
        </form>
    </div>
</div>
<?php include_once("./Config/ImportJs.php"); ?>
</html>
