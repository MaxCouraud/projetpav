<?php
namespace App;

include_once("./Config/Header.php"); ?>

<html>

<?php include_once("./Components/Navbar.php");?>

<div>
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link" href="AdminPage.php">Créer Pav</a>
        </li>
        <li class="nav-item">
            <a class="nav-link active" href="AdministrerPav.php">Administer Pav</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="CreerAgent.php">Créer Agent</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="AdministrerAgent.php">Administer Agent</a>
        </li>
    </ul>
</div>
<?php include_once("./Config/ImportJs.php"); ?>
</html>