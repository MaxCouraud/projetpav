<?php
namespace App;

include_once("./Config/Header.php");
include_once("./Components/Navbar.php"); ?>

<body>
<div class="jumbotron">
    <h1 class="display-4">Projet Gestion PAV</h1>
    <p class="lead">Développer une application de collecte de relevés pour éviter d'imprimer chaque jour les tournées à effectuer et récupérer les feuillets en fin de tournées.
        Application permettant à un technicien itinérant de collecter des données sur site depuis un smartphone ou Tablette.</p>
    <hr class="my-4">
    <p>Gérer des PAV (Point d'Apport Volontaire) et les agents de collecte, c’est-à-dire :</p>
    <ul>
        <li>Créer un PAV, le modifier, le supprimer. Les Affecter à un numéro de tournée (de 1 à n) et une date de tournée.</li>
        <li>Créer un Agent, le modifier, le supprimer.</li>
        <li>Lister les Agents.</li>
        <li>Lister les PAV pour une tournée. Imprimer les PAV de la tournée après les relevés.</li>
        <li>Un PAV est caractérisé par : un ID, un nom (Ville – Quartier), Taux de remplissage (0/4, 1/4, 2/4, 3/4, 4/4), Date de relevé, ID agent, Commentaire).</li>
        <li>Un agent est caractérisé par un ID, un nom et prénom.</li>
    </ul>
    <hr class="my-4">
    <p>A développer :</p>
    <ul>
        <li>Application BackOffice de gestion Agent et PAV</li>
        <li>Application FrontOffice de relevés de PAV.</li>
        <li>Un agent est caractérisé par un ID, un nom et prénom.</li>
    </ul>
    <hr class="my-4">
    <p>Divers :</p>
    <ul>
        <li>Environnement : PHP, HTML</li>
        <li>Identification par mot de passe non nécessaire mais souhaitée.</li>
        <li>Exemple de rapport rempli à la main (page suivante)</li>
    </ul>
    <div class="row">
        <div class="col-12 col-sm-1">
            <a class="btn btn-outline-primary btn-lg" href="Connexion.php" role="button">Connexion</a>
        </div>
    </div>
</div>
</body>
<?php include_once("./Config/ImportJs.php"); ?>
</html>
