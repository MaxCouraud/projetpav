<?php
namespace App;

include_once("./Config/Header.php"); ?>

<html>

<?php include_once("./Components/Navbar.php");

//connexion à la bdd
$data = new \PDO('mysql:dbname=pav;host=localhost', 'root', '');

//générer les erreurs, sans cette phrase pdo ne renvoie pas l'exception..
$data->setAttribute(\PDO::ATTR_ERRMODE,  \PDO::ERRMODE_EXCEPTION);

//formatage donnée
$data->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ);

if(!empty($_POST) && !empty($_POST['email']) && !empty($_POST['password'])){
    $req = $data->prepare('SELECT * FROM pav_user WHERE email = :email');
    $req->execute(['email' => $_POST['email']]);
    $user = $req->fetch();
    $utilisateurConnecte = $user->type_user;
    if($utilisateurConnecte==2){
        header('Location: AdminPage.php');
    }elseif ($utilisateurConnecte==3){
        header('Location: PageAgent.php');
    }else{
        echo("Ce compte n'existe pas");
    }
}
?>

<div>
    <div class="row justify-content-md-center">
        <div class="col-sm-4" style="padding-top: 2%;">
            <h1>Se Connecter</h1>
            <form action="" method="POST">
                <div  class="form-group">
                    <label for="">Email</label>
                    <input type="email" name="email" class="form-control" placeholder="Entrez votre email" required>
                </div>
                <div  class="form-group">
                    <label for="">Mot de passe</label>
                    <input type="password" name="password" class="form-control" placeholder="Entrez votre mot de passe" required>
                </div>

                <button type="submit" class="btn btn-primary" >Se connecter</button>
            </form>
        </div>
    </div>
</div>
<?php include_once("./Config/ImportJs.php"); ?>
</html>
