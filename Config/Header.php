<?php
namespace App\Projet\Config;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>ProjetPav</title>
        <link rel="icon" href="https://www.pngfind.com/pngs/m/577-5778269_this-free-icons-png-design-of-parking-15.png" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="../Style/style.css"/>
    </head>
</html>