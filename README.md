# Projet Pav          
https://gitlab.com/MaxCouraud/projetpav     
### Description du projet :
Ce projet à pour but de former au HTML, PHP et SQL par le biais d'un système d'inscription et de connexion.
### Guide du démarrage :
- [Wamp](http://www.wampserver.com/) : Pour windows comprenant un serveur Apache2, du langage de scripts PHP et d’une base de données MySQL. Il possède également PHPMyAdmin pour gérer plus facilement vos bases de données.
 >Il est important de mettre le ProjetPav comme tous vos projets php dans le fichier wamp64/wwww  
- [Mamp](https://www.mamp.info/en/) : Contient les mêmes élèments que Wamp mais s'adresse au système d'exploitation Mac.

- Lamp : Pour linux, l'installation se fait manuellement.
  - Installation des paquets :
  ```sh
  sudo apt install apache2 php libapache2-mod-php mysql-server php-mysql
  ```
  - Pour pouvoir lancer des scripts SQL : 
  ```sh
    sudo mysql
  ```
  
Vous trouverez donc le projet à l'adresse suivante sur votre navigateur :
```sh
localhost/ProjetPav/
```

### Configuration de la base de donnée :
Il nous faut d'abord créer la base de donnée pour cela phpMyAdmin est très utile, je vous propose toute fois de créer les éléments de la base de données par script sql. Rendez vous à l'adresse :
```sh
http://localhost/phpmyadmin/
```
- Script génération base de donnée :
```sh
CREATE DATABASE pav
```
- Créer la table :
```sh
CREATE TABLE pav_user
(
    id INT PRIMARY KEY NOT NULL,
    nom VARCHAR(100),
    prenom VARCHAR(100),
    email VARCHAR(255),
    password VARCHAR(25),
    ville VARCHAR(255),
    code_postal VARCHAR(5),
    type_user INT
)
```
Vous pouvez désormais vos connecter sur cette interface web :)

### Evolution de l'application

- Une partie inscription, avec confirmation par email des comptes utilisateurs.
- Une partie connexion / déconnexion, avec une option "Se souvenir de moi" basée sur l'utilisation des cookies.
- Une option "rappel du mot de passe" pour les utilisateurs un petit peu tête en l'air.
- Une partie réservé aux membres avec la possibilités de changer son mot de passe.
- Et en dernière étape refaire tout le projet en POO.

### Installation sur projet avec Git

Si vous etes sur windows il vous faut installer [git pour windows](https://gitforwindows.org/) pour pouvoir reconnaitre les commandes git. Si vous êtes sur mac ou linux lancez la commande :
```sh
sudo apt install git
```
Vous pouvrez vérifiez que vous avez bien git avec la commande :
```sh
git --version
```
Une fois git installé rendez vous sur la page Gitlab du projet et lancez la commande :
```sh
git clone https://gitlab.com/MaxCouraud/projetpav.git
```
### Auteur

<strong>Maxime Couraud</strong>

### Crédits

Je remercie Graphikart et son sens de la pédagogie. Mon cours en est largement inspiré.
