<?php
namespace App;

include_once("./Config/Header.php"); ?>

<html>

<?php include_once("./Components/Navbar.php");

//connexion à la bdd
$data = new \PDO('mysql:dbname=pav;host=localhost', 'root', '');

//générer les erreurs, sans cette phrase pdo ne renvoie pas l'exception..
$data->setAttribute(\PDO::ATTR_ERRMODE,  \PDO::ERRMODE_EXCEPTION);

//formatage donnée
$data->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ);

?>
<div>
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link" href="AdminPage.php">Créer Pav</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="AdministrerPav.php">Administer Pav</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="CreerAgent.php">Créer Agent</a>
        </li>
        <li class="nav-item">
            <a class="nav-link active" href="AdministrerAgent.php">Administer Agent</a>
        </li>
    </ul>
</div>
<?php

$query = $data->query("SELECT nom, prenom, email FROM pav_user");

$recup = $query->fetch();

var_dump($recup);
?>
<?php include_once("./Config/ImportJs.php"); ?>
</html>


